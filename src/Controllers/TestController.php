<?php

namespace App\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class TestController
{
    public static function Test(Request $request, Response $response, array $args): Response {
        $data = file_get_contents('data.txt');

        file_put_contents('data.txt', $data . '\nVasya');

        return $response->withHeader('Content-type', 'application/json');
    }
}
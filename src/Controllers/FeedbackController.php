<?php

namespace App\Controllers;

use App\Repository\FeedbackRepository;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class FeedbackController
{
    public function __construct(
        private FeedbackRepository $repository
    ) {}

    public function getFeedbackById(Request $request, Response $response, array $args): Response {
        try {
            $response->getBody()->write(json_encode($this->repository->getFeedbackById($args['id'])));
        } catch (\Exception $e) {
            throw new \Exception('dsa', 0, $e);
        }
        return $response->withHeader('Content-type', 'application/json');
    }

    public function getFeedbacks(Request $request, Response $response, array $args): Response {
        $response->getBody()->write(json_encode($this->repository->getFeedbacks($request->getQueryParams())));
        return $response->withHeader('Content-type', 'application/json');
    }

    public function createFeedback(Request $request, Response $response, array $args): Response {
        $result = $this->repository->createFeedback($request->getParsedBody());
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    public function deleteFeedback(Request $request, Response $response, array $args): Response {
        $this->repository->deleteFeedback($args['id']);
        return $response;
    }
}
<?php

use App\Containers\CustomErrorHandler;
use App\Controllers\FeedbackController;
use App\Controllers\HomeController;
use App\Repository\FeedbackRepository;
use App\Services\DB;
use Slim\App;
use Slim\Views\PhpRenderer;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);

require __DIR__ . '/vendor/autoload.php';

$config = (require __DIR__ . '/settings.php');

$app = new App(['settings' => $config['settings']]);

$c = $app->getContainer();
$c['errorHandler'] = new CustomErrorHandler();
$c['DB'] = new DB(new SQLite3($config['db']['path_file']));
//$c['BasicAuthenticationMiddleware'] = new BasicAuthenticationMiddleware($config['admin']);
$c['view'] = new PhpRenderer("public/");

$c['repository'] = function ($c) {
    return new FeedbackRepository($c->get('DB'));
};

$c['FeedbackController'] = function ($c) {
    return new FeedbackController($c->get('repository'));
};

$c['HomeController'] = function ($c) {
    return new HomeController($c->get('view'));
};

$app->get('/', 'HomeController');
$app->post('/api/log',  function (Request $request, Response $response, array $args): Response {
    $data = file_get_contents('data.txt');

    file_put_contents('data.txt', $data . "\nVasya");

    echo 1;

    return $response;
});
$app->get('/api/feedbacks/{id}', 'FeedbackController:getFeedbackById');
$app->get('/api/feedbacks', 'FeedbackController:getFeedbacks');
$app->post('/api/feedbacks', 'FeedbackController:createFeedback');
//$app->get('/api/feedbacks/delete/{id}', 'FeedbackController:deleteFeedback')->add($c->get('BasicAuthenticationMiddleware'));

$app->run();
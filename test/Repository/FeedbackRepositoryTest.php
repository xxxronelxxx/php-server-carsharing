<?php

namespace App\Repository;

use App\Services\DB;
use PHPUnit\Framework\TestCase;

class FeedbackRepositoryTest extends TestCase
{
    public function testGetFeedbackById_normal(): void
    {
        $db = $this->createMock(DB::class);

        $db->expects($this->once())
            ->method('select')
            ->with("SELECT * FROM feedbacks WHERE id = :id", [":id" => 2])
            ->willReturn(array(
                array(
                    "id" => 2,
                    "user_name" => "Test",
                    "article" => "Test",
                    "date_created" => "2022-10-28"
                )
            ));

        $repository = new FeedbackRepository($db);

        $result = $repository->getFeedbackById(2);

        $this->assertEquals(array(
            "id" => 2,
            "user_name" => "Test",
            "article" => "Test",
            "date_created" => "2022-10-28"
        ), $result);
    }

    public function testGetFeedbackById_feedbackNotFound() {
        $db = $this->createMock(DB::class);

        $db->expects($this->once())
            ->method('select')
            ->with("SELECT * FROM feedbacks WHERE id = :id", [":id" => 2])
            ->willReturn(array());

        $repository = new FeedbackRepository($db);

        $this->expectException(\Exception::class);

        $repository->getFeedbackById(2);
    }

    public function testGetFeedbacks__normal() {
        $db = $this->createMock(DB::class);

        $db->expects($this->exactly(2))
            ->method('select')
            ->withConsecutive(
                array(
                    'SELECT * FROM feedbacks ORDER BY id DESC LIMIT 0, 10'
                ),
                array(
                    'SELECT COUNT(*) as count FROM feedbacks'
                )
            )
            ->willReturnOnConsecutiveCalls(
                array(
                    array(
                        "id" => 2,
                        "user_name" => "Test",
                        "article" => "Test",
                        "date_created" => "2022-10-28"
                    )
                ),
                array(
                    array(
                        'count' => 1
                    )
                )
            );

        $repository = new FeedbackRepository($db);

        $result = $repository->getFeedbacks(array(
            "page" => 1
        ));

        $this->assertEquals(array(
            "data" => array(
                array(
                    "id" => 2,
                    "user_name" => "Test",
                    "article" => "Test",
                    "date_created" => "2022-10-28"
                )
            ),
            "total" => 1,
            "page" => 1
        ), $result);
    }

    public function testGetFeedbacks__pageNotFound() {
        $db = $this->createMock(DB::class);

        $repository = new FeedbackRepository($db);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Параметр page не найден");

        $result = $repository->getFeedbacks(array());
    }

    public function testGetFeedback__pageNotNumberOrLessEqualsZero() {
        $db = $this->createMock(DB::class);

        $repository = new FeedbackRepository($db);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Параметр page введен неправильно");

        $result = $repository->getFeedbacks(array(
            "page" => "dvnfjv"
        ));
    }

    public function testCreateFeedback__normal() {
        $db = $this->createMock(DB::class);

        $repository = new FeedbackRepository($db);

        $db->expects($this->once())
            ->method('insert')
            ->with("INSERT INTO feedbacks (article, user_name, date_created) VALUES ('test','test','2022-10-31');")
            ->willReturn(4);

        $result = $repository->createFeedback(array(
            "article" => "test",
            "user_name" => "test"
        ));

        $this->assertEquals(4, $result);
    }

    public function testCreateFeedback__notParams() {
        $db = $this->createMock(DB::class);

        $repository = new FeedbackRepository($db);

        $this->expectException(\Exception::class);

        $result = $repository->createFeedback(array());
    }

    public function testDeleteFeedback__normal() {
        $db = $this->createMock(DB::class);

        $db->expects($this->once())
            ->method('select')
            ->with("SELECT * FROM feedbacks WHERE id = :id", [":id" => 2])
            ->willReturn(array(
                array(
                    "id" => 2,
                    "user_name" => "Test",
                    "article" => "Test",
                    "date_created" => "2022-10-28"
                )
            ));

        $db->expects($this->once())
            ->method('delete')
            ->with('DELETE FROM feedbacks WHERE id = :id', [":id" => 2]);

        $repository = new FeedbackRepository($db);

        $repository->deleteFeedback(2);


    }

    public function testDeleteFeedback__notFoundFeedback() {
        $db = $this->createMock(DB::class);

        $db->expects($this->once())
            ->method('select')
            ->with("SELECT * FROM feedbacks WHERE id = :id", [":id" => 2])
            ->willReturn(array());

        $repository = new FeedbackRepository($db);

        $this->expectException(\Exception::class);

        $repository->deleteFeedback(2);
    }


}
<?php

namespace App\Services;


class DB
{
    private \SQLite3 $db;

    public function __construct(\SQLite3 $db) {
        $this->db = $db;
    }

    public function select(string $sql, array $params = []): ?array {
        $resultArray = [];
        $result = $this->query($sql, $params);

        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $resultArray[] = $row;
        }

        return $resultArray;
    }

    public function insert(string $sql, array $params = []): int
    {
        $this->query($sql, $params);
        return $this->db->lastInsertRowID();
    }

    public function delete(string $sql, array $params = []): void {
        $this->query($sql, $params);
    }


    private function query(string $sql, array $params = []): \SQLite3Result {
        $stmt = $this->db->prepare($sql);

        foreach ($params as $key => $elem) {
            $stmt->bindParam($key, $elem);
        }
        $result = $stmt->execute();

        if ($result === false) {
            throw new \Exception("Неправильный запрос");
        }

        return $result;
    }
}
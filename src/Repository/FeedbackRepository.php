<?php

namespace App\Repository;
use App\Services\DB;

class FeedbackRepository
{
    const PAGE_SIZE = 10;

    public function __construct(
        private readonly DB $db
    ){}

    /**
     * @throws \Exception
     */
    public function getFeedbackById(int $id): ?array {
        $result = $this->db->select("SELECT * FROM feedbacks WHERE id = :id", [":id" => $id]);
        if (!isset($result[0])) {
            throw new \Exception("Такого отзыва не существует");
        }

        return $result[0];
    }

    /**
     * @throws \Exception
     */
    public function getFeedbacks(array $args): ?array {
        if (!isset($args['page'])) {
            throw new \Exception("Параметр page не найден");
        }
        $page = (int)$args['page'];
        if ($page <= 0){
            throw new \Exception("Параметр page введен неправильно");
        }

        return [
            'data' => $this->db->select('SELECT * FROM feedbacks ORDER BY id DESC LIMIT '. ($page - 1) * self::PAGE_SIZE .', '. $page * self::PAGE_SIZE),
            'total' => $this->db->select('SELECT COUNT(*) as count FROM feedbacks')[0]['count'],
            'page' => $page
        ];
    }

    public function createFeedback(array $feedback): int {
        if (!isset($feedback['article']) || !isset($feedback['user_name']) ) {
            throw new \Exception("Неправильные данные");
        }
        $feedback['date_created'] = date("Y-m-d");

        return $this->db->insert(
            "INSERT INTO feedbacks (article, user_name, date_created) VALUES ('".
            $feedback["article"] ."','". $feedback["user_name"] ."','" . $feedback["date_created"] . "');"
        );
    }

    public function deleteFeedback(int $id): void {
        $this->getFeedbackById($id);

        $this->db->delete('DELETE FROM feedbacks WHERE id = :id', [":id" => $id]);
    }
 }
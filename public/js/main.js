const base_url = '/api/feedbacks'
let page = 1, total = 0;

async function getFeedbacks(page_current) {
    let result = await request(`${base_url}?page=${page_current}`, {'Content-type': 'application/json'})
    let res = await result.json()

    if (res.data.length === 0) {
        await getFeedbacks(page_current - 1)
        return;
    }

    total = res.total
    page = Number(res.page)

    renderFeedback(res.data)
    renderPaginationElems(Math.ceil(total / 10))
    activePaginationElem(page)
}

async function createFeedback() {
    let user_name = document.querySelector('#user_name').value
    let article = document.querySelector('#article').value
    if (user_name === "" || article === "") {
        showAlert('Не введены данные', false)
        return
    } else {
        hideAlert(false)
        await request(base_url, {
            method: 'POST',
            body: JSON.stringify({user_name, article}),
            headers: { 'Content-type': 'application/json' }
        })
        page = 1
        await getFeedbacks(page)

        document.querySelector('#user_name').value = ""
        document.querySelector('#article').value = ""
    }
}

function renderFeedback(data) {
    let list = document.querySelector('.feedback-list__content')
    list.innerHTML = ""
    data.forEach((elem) => {
        list.innerHTML += layoutFeedback(elem)
    })
}

function layoutFeedback(data) {
    return `
    <div class="feedback-list__item">
        <div class="feedback-list__item-name">Автор: <span>${data.user_name}</span></div>
        <div class="feedback-list__item-article">${data.article}</div>
        <div class="feedback-list__date">Дата создания: ${data.date_created}</div>
        <div class="feedback-list__delete-btn btn" data-id="${data.id}">Удалить</div>
    </div>
    `
}

function showAlert(message, isGlobal = true) {
    let selector = isGlobal ? '#global-error' : '#feedback-add__alert';
    document.querySelector(selector).innerHTML = message
    document.querySelector(selector).style.visibility = 'visible'
}

function hideAlert(isGlobal = true) {
    let selector = isGlobal ? '#global-error' : '#feedback-add__alert';
    document.querySelector(selector).style.visibility = 'hidden'
}

async function request(page, init) {
    try {
        hideAlert()
        return await fetch(page, init)
    } catch (error) {
        showAlert('Ошибка запроса')
        throw new Error(error)
    }
}

document.getElementById('feedback-add').onclick = async () => {
    await createFeedback()
}

function hasClass(elem, className) {
    return elem.classList.contains(className);
}

function activePaginationElem(page) {
    document.querySelectorAll('.feedback-pagination__item').forEach(elem => {
        elem.classList.remove('active')
    })
    document.querySelectorAll('.feedback-pagination__item')[page - 1].classList.add('active')

    document.querySelector('#feedback_back').disabled = page === 1
    document.querySelector('#feedback_next').disabled = page === Math.ceil(total / 10)

}

function renderPaginationElems(total) {
    let list = document.querySelector('.feedback-pagination__content')
    list.innerHTML = ""
    for (let i = 1; i <= total; i++) {
        list.innerHTML += `<div class="feedback-pagination__item active">${i}</div>`
    }
}

document.addEventListener('click', async function (e) {
    if (hasClass(e.target, 'feedback-list__delete-btn')) {
        let id = e.target.attributes.getNamedItem('data-id').value
        await request(`${base_url}/delete/${id}`, {})
        await getFeedbacks(page)
    } else if (hasClass(e.target, 'feedback-pagination__btn')) {
        await getFeedbacks(e.target.id === 'feedback_back' ? page - 1 : page + 1)
    }
}, false);

(async () => {
    await getFeedbacks(page)
})()

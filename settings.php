<?php

return [
    'db' => [
        'path_file' => 'DB.sqlite'
    ],
    'settings' => [
        'displayErrorDetails' => false,
        'addContentLengthHeader' => false,
        'templates.path' => 'public'
    ],
    'admin' => [
        'login' => 'admin',
        'password' => 'root'
    ]
];
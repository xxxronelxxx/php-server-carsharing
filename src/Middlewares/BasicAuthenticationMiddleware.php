<?php

namespace App\Middlewares;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class BasicAuthenticationMiddleware
{
    public function __construct(
        private readonly array $config
    ){}

    public function __invoke(Request $request, Response $response, $next): Response
    {
        if (isset($request->getHeader("PHP_AUTH_USER")[0])
            && isset($request->getHeader("PHP_AUTH_USER")[0])) {

            if ($request->getHeader("PHP_AUTH_USER")[0] == $this->config["login"]
                && $request->getHeader("PHP_AUTH_PW")[0] == $this->config["password"]) {

                return $next($request, $response);
            }
        }

        return $response
            ->withHeader('WWW-Authenticate', 'Basic realm="test"')
            ->withStatus(401, 'Unauthorized');
    }
}
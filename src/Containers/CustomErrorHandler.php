<?php

namespace App\Containers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class CustomErrorHandler
{
    public function __invoke(Request $request, Response $response, \Exception $exception) {
        $response->getBody()->write(json_encode([ 'message' => $exception->getMessage() ], JSON_UNESCAPED_UNICODE));
        return $response
            ->withStatus(500)
            ->withHeader('Content-Type', 'application/json; charset=UTF-8');
    }
}